open Creal;;
open Infixes;;

print_endline (
  to_string 
    (match int_of_string (Sys.argv.(2)) with
      | 1-> sin(tan(cos one))
      | 2 -> sqrt (e /! pi)
      | 3 -> sin (pow_int (e +! one) 3)
      | 4 -> pow e (pi *! sqrt (of_int 2011)) 
      | 5 -> pow e (pow e (sqrt e))
      | 6 -> arctanh (one -! arctanh (one -! arctanh (one -! arctanh (one/!pi))))
      | 7 -> pow_int pi 1000
      | 8 -> sin (of_float (6.**(6.**6.)))
      | 9 -> sin (of_int 10 *! arctanh(tanh(pi *! sqrt(of_int 2011) /! of_int 3)))
      | 10 -> root 3 (of_int 7 +! root 5 (of_int 2) -! of_int 5 *! root 5 (of_int 8)) +! root 5 (of_int 4) -! root 5 (of_int 2)
      | 11 -> tan (sqrt two) +! arctanh(sin one)
      | 12 -> arcsin(inv e) +! cosh e +! arctanh e
      | _ -> failwith "unknown problem number"
    )
    (int_of_string Sys.argv.(1))
);;
