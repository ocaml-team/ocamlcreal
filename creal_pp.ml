
open Format

let precision = ref 20;;

let pp x = print_string (try Creal.to_string x !precision 
			 with e -> "Error: " ^ Printexc.to_string e);;

