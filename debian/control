Source: ocamlcreal
Section: ocaml
Priority: optional
Maintainer: Debian OCaml Maintainers <debian-ocaml-maint@lists.debian.org>
Uploaders:
 Mehdi Dogguy <mehdi@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 ocaml,
 libgmp-ocaml-dev,
 dh-ocaml
Standards-Version: 4.6.0
Rules-Requires-Root: no
Vcs-Git: https://salsa.debian.org/ocaml-team/ocamlcreal.git
Vcs-Browser: https://salsa.debian.org/ocaml-team/ocamlcreal

Package: libcreal-ocaml-dev
Architecture: any
Depends:
 ${ocaml:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Provides:
 ${ocaml:Provides}
Description: O'Caml library that implements exact real arithmetic
 This  module  implements  exact  real  arithmetic,  following  Valerie
 Menissier-Morain Ph.D. thesis (http://www-calfor.lip6.fr/~vmm/).
 .
 A  real  x  is  represented  as  a function  giving,  for  any  n,  an
 approximation zn/4^n of x  such that |zn/4^n - x| < 1,  where zn is an
 arbitrary precision integer (of type Gmp.Z.t).
 .
 Coercions from type int, Gmp.Z.t, Gmp.Q.t, basic operations (addition,
 subtraction,  multiplication,   division,  power,  square   root)  and
 transcendental  functions (sin,  cos, tan,  log, exp,  arcsin, arccos,
 etc.) and a few constants (pi, e) are provided.
 .
 A small reverse-polish calculator is provided to test the library.
