
open Graphics

let pi = 4. *. atan 1.
let xmin = -9.
let xmax =  9.
let ymin = -1.
let ymax =  1.

let creal = ref false
let () = Arg.parse ["-creal", Arg.Set creal, "";
		    "-cr", Arg.Clear creal, ""] (fun _ -> ()) ""
let f_creal x =
  Creal.to_float (Creal.sin (Creal.of_float x)) 3
let f_cr x = 
  Cr.to_float (Cr.sin (Cr.of_float x)) 6
let f = if !creal then f_creal else f_cr

let w = 800
let h = 600
let g = Printf.sprintf " %dx%d" w h
let () = open_graph g; set_color black

let one_pixel = (xmax -. xmin) /. float w

let dx = xmax -. xmin
let dy = ymax -. ymin
let i x = truncate (float w *. (x -. xmin) /. dx)
let j y = truncate (float h *. (y -. ymin) /. dy)

let moveto x y = moveto (i x) (j y)
let lineto x y = lineto (i x) (j y)
let plot x y = plot (i x) (j y)

let () =
  moveto 0. ymin;
  lineto 0. ymax;
  moveto xmin 0.;
  lineto xmax 0.

let () = 
  for i = 0 to w - 1 do
    let x = xmin +. float i *. one_pixel in
    let y = f x in
    Graphics.plot i (j y)
  done

let _ = wait_next_event [Key_pressed]
